package org.isima.javapro;

import java.util.ArrayList;
import java.util.List;

public class LdapSimulator {

    public static String[] employees = {"newton", "galileo"};

    public static boolean isExisting(String employee){
        return (employee.matches(employees[0]) || employee.matches(employees[1]));
    }
}
