package org.isima.javapro.valid;


import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.isima.javapro.LdapSimulator;
import org.isima.javapro.Parser;
import org.isima.javapro.expense.Expense;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingDouble;

public class ExpenseValidator {

    private static String EXPENSE_FILE_URL = "https://raw.githubusercontent.com/anthoRx/td1/master/expenses.csv";

    private static String HISTORY_FILE = "/history/valid_expenses.json";
    private static String INVALID_HISTORY_FILE = "/history/invalid_expenses.json";

    public static boolean haveLegalAmount(Expense e) {
        return e.getAmount() > 0 && e.getAmount() < Double.MAX_VALUE;
    }

    public static boolean havePolicyAmount(Expense e) {
        Map<String, Double> policy = new HashMap<>();
        policy.put("hotel", 180.0);
        policy.put("repas", 35.0);
        policy.put("train", 200.0);
        policy.put("avion", 800.0);
        policy.put("drinks", 100.0);

        Double policyValue = policy.get(e.getCategory());
        return policyValue == null || policyValue > e.getAmount();
    }

    private static boolean respectMonthPolicy(List<Expense> expenses) {
        double limit = 1000;
        return expenses.stream()
                .collect(groupingBy(Expense::getMonth, summingDouble(Expense::getAmount)))
                .entrySet()
                .stream()
                .allMatch(entry -> entry.getValue() < limit);
    }


    public void validate() {
        List<Expense> validExpenses = new ArrayList<>();
        String toFile = "/tmp/expenses.csv";

        try {
            FileUtils.copyURLToFile(new URL(EXPENSE_FILE_URL), new File(toFile), 10000, 10000);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parser parser = new Parser(toFile);
        parser.parse();
        validExpenses = parser.getValidExpenses();
        final List<Expense> invalidExpenses = parser.getInvalidExpenses();




        Map<String, List<Expense>> expensesByEmployee = validExpenses.stream().collect(groupingBy(Expense::getEmployee));
        List<Expense> expensesToSave = new ArrayList<>();
        expensesByEmployee.forEach((k, v) -> {
            if(respectMonthPolicy(v))
                expensesToSave.addAll(v);
            else
                invalidExpenses.addAll(v);
        });


        expensesToSave.forEach(expense -> {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonString = objectMapper.writeValueAsString(expense);
                File historyFile = new File(INVALID_HISTORY_FILE);
                if (!Files.exists(historyFile.getParentFile().toPath()))
                    Files.createDirectories(historyFile.getParentFile().toPath());
                if(historyFile.exists())
                    Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.APPEND);
                else
                    Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.CREATE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        invalidExpenses.forEach(expense -> {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonString = objectMapper.writeValueAsString(expense);
                File historyFile = new File(HISTORY_FILE);
                if (!Files.exists(historyFile.getParentFile().toPath()))
                    Files.createDirectories(historyFile.getParentFile().toPath());
                if(historyFile.exists())
                    Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.APPEND);
                else
                    Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.CREATE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    private boolean isEmployeeExists(String employee) {
        return LdapSimulator.isExisting(employee);
    }

}
