package org.isima.javapro;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.isima.javapro.expense.Expense;
import org.isima.javapro.valid.ExpenseValidator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Parser {

    private String toFile;
    private CSVFormat csvFormat;
    private DateTimeFormatter dateFormatter;
    private List<Expense> validExpenses;
    private List<Expense> invalidExpenses;

    public Parser(String toFile){

        this.toFile = toFile;

        csvFormat = CSVFormat.DEFAULT
                .withHeader("id", "employee", "date", "category", "description", "amount")
                .withDelimiter(';')
                .withTrim();

        dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        validExpenses = new ArrayList<>();
        invalidExpenses = new ArrayList<>();
    }

    public void parse(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(toFile));
            CSVParser csvParser = new CSVParser(reader, csvFormat);

            for (CSVRecord csvRec : csvParser) {
                if (!"id".equals(csvRec.get("id"))) {
                    LocalDate date = LocalDate.parse(csvRec.get("date"), dateFormatter);
                    double amount = Double.parseDouble(csvRec.get("amount"));

                    Expense expense = new Expense(csvRec.get("id"), csvRec.get("employee"), date, csvRec.get("category"), csvRec.get("description"), amount);

                    if (LdapSimulator.isExisting(expense.getEmployee()) && ExpenseValidator.haveLegalAmount(expense) && ExpenseValidator.havePolicyAmount(expense))
                        validExpenses.add(expense);
                    else
                        invalidExpenses.add(expense);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Expense> getValidExpenses(){
        return validExpenses;
    }

    public List<Expense> getInvalidExpenses(){
        return invalidExpenses;
    }

}
